firebase.auth().onAuthStateChanged(function(user) {
  if (user) {
  // User is signed in.

    document.getElementById("user_div").style.display = "block";
    document.getElementById("loggedout").style.display = "block";
    document.getElementById("scientists").style.display="block";
    document.getElementById("login_div").style.display = "none";
    document.getElementById("loggedin").style.display = "none";
    document.getElementById("nav_bar").style.display = "none";
    document.getElementById("dataView").style.display="none";

  //Gets users profile picture
  var user = firebase.auth().currentUser;
  var storageRef = firebase.storage().ref();
  var spaceRef = storageRef.child('users/'+user.uid+'.jpg');
  var firstnameRef = firebase.database().ref().child('users/'+user.uid+'/firstname');
  var lastnameRef = firebase.database().ref().child('users/'+user.uid+'/lastname');
  var user_title = document.getElementById("user_name");

  if(user != null){
    var email_id = user.email;
      
    spaceRef.getDownloadURL().then(function(url) {
        var test = url;
        // alert(url);
        document.querySelector('img').src = test;

    }).catch(function(error) {

    });
  }

  //Get users first and last name
  var fname;
  var lname;
  firstnameRef.on('value', function(datasnapshot) {
    fname = datasnapshot.val();
    document.getElementById("forename").innerHTML = fname;
  });

  lastnameRef.on('value', function(datasnapshot) {
    lname = datasnapshot.val();
    document.getElementById("surname").innerHTML = lname;
  });

  //Account Type
  var acc_type;

  } else {
    // No user is signed in.
    document.getElementById("user_div").style.display = "none";
    document.getElementById("loggedout").style.display = "none";
    document.getElementById("login_div").style.display = "block";
    document.getElementById("loggedin").style.display = "block";
    document.getElementById("nav_bar").style.display = "block";
  }
});

var coords = [];

function data() {
  document.getElementById("dataView").style.display="block";
  document.getElementById("scientists").style.display="none";

  var recordRef = firebase.database().ref().child('recordings/');
  // var sb = [];
  var data = "<table><tr><th>Date</th><th>Time</th>" +
  "<th>Animals</th><th>Average Depth</th><th>Bog</th><th>Clarity</th><th>Cobbles</th>" +
  "<th>Comments</th><th>Crops</th><th>Forest</th><th>Grassland</th><th>Gravel</th>" +
  "<th>Location</th><th>More Natural</th><th>Mud Depth</th><th>Riffle</th>" +
  "<th>Rubbish</th><th>Samplers</th><th>Sand</th><th>Shading</th>" +
  "<th>Silt or Mud</th><th>Siltation</th><th>Stream Description</th>" +
  "<th>Stream Name</th><th>Stream Width</th><th>Suburban</th><th>Tillage</th>" +
  "<th>Trees</th><th>Urban</th><th>Velocity</th><th>Wet Width</th><th colspan='25'>Insect Sample</th>";

  var tdCount = 0;
  var dateFormat = "DD-MM-YYYY";
  var value = "";
  var keyCount = 0;

  // Section = [0, details], [1, environment], [2, insects]
  var section = 0;

  // Getting data from Firebase
  recordRef.on('value', function(snapshot) {
    snapshot.forEach(function(snapshot1) {
      snapshot1.forEach(function(snapshot2) {
        snapshot2.forEach(function(snapshot3) {
          value = snapshot3.val();

          // if(moment(value, dateFormat, true).isValid()) {
          //   keyCount = 0;
          // }
          // else {
          //   keyCount++;
          // }

          // Get key(insect name) 
          var key = Object.keys(snapshot2.val())[keyCount];
          data = concat(data, key, value, section);
          keyCount++;

          document.getElementById("dataTable").innerHTML = data;

        });
        keyCount = 0;
        section++;
      });
      section = 0;
    });

    initMap();
  }); 
  document.getElementById("dataTable").innerHTML += "</table>";

}

function concat(sb, key, value, section) {
  if(section === 0) {
    if(key === "uid") { // Skip UID
      return sb;
    }
    else if(key === "date") { // New row
      sb += "</tr><td>" + value + "</td>";
    }
    else {
      sb += "<td>" + value + "</td>";
    }
  }
  else if(section === 2) { // Insect name and value
    sb += "<td>" + key + ": " + value + "</td>";
  }
  else {
    sb += "<td>" + value + "</td>";
  }

  // Store location value in coords array
  if(key === "Location") {
    coords.push(value);
  }

  return sb;
}

// Initialise heatmap
function initMap() {
  map = new google.maps.Map(document.getElementById('heatmap'), {
    zoom: 12,
    center: {lat: 53.98, lng: -6.40},
    mapTypeId: 'satellite'
  });

  heatmap = new google.maps.visualization.HeatmapLayer({
    data: getPoints(),
    map: map
  });
}

function toggleHeatmap() {
  heatmap.setMap(heatmap.getMap() ? null : map);
}

// Check if object or array
function isArray(o) {
  return Object.prototype.toString.call(o) === "[object Array]";
}

function getPoints() {
  var heatmapData = [];
  var lat = [];
  var lon = [];
  var coord;

  var len = coords.length;

  for (var i = 0;  i < len; i++) {
    coord = coords[i].split(", ");
    
    lat.push(coord[0]);
    lon.push(coord[1]);

    heatmapData.push(new google.maps.LatLng(lat[i], lon[i]));
  }

  return heatmapData;
}

function changeGradient() {
  var gradient = [
    'rgba(0, 255, 255, 0)',
    'rgba(0, 255, 255, 1)',
    'rgba(0, 191, 255, 1)',
    'rgba(0, 127, 255, 1)',
    'rgba(0, 63, 255, 1)',
    'rgba(0, 0, 255, 1)',
    'rgba(0, 0, 223, 1)',
    'rgba(0, 0, 191, 1)',
    'rgba(0, 0, 159, 1)',
    'rgba(0, 0, 127, 1)',
    'rgba(63, 0, 91, 1)',
    'rgba(127, 0, 63, 1)',
    'rgba(191, 0, 31, 1)',
    'rgba(255, 0, 0, 1)'
  ]
  heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
}

function changeRadius() {
  heatmap.set('radius', heatmap.get('radius') ? null : 20);
}

function changeOpacity() {
  heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
}

// CHART
// Load the Visualization API and the corechart package.
google.charts.load('current', {'packages':['corechart']});

// Set a callback to run when the Google Visualization API is loaded.
google.charts.setOnLoadCallback(drawChart);

// Callback that creates and populates a data table,
// instantiates the pie chart, passes in the data and draws it.
function drawChart() {
	// Create the data table.
	var data = new google.visualization.DataTable();
	data.addColumn('string', 'Test');
	data.addColumn('number', 'Insect');
	data.addRows([
	  ['Caenis', 3],
	  ['Mayfly', 6],
	  ['Stonefly', 2],
	  ['Caddis', 1.5]
	]);

	// Set chart options
	var options = {'title':'Insects found in River Glyde'};

// Instantiate and draw our chart, passing in some options.
var chart = new google.visualization.BarChart(document.getElementById('chart_div'));
chart.draw(data, options);
}

// Convert to CSV
function exportTableToCSV() {
  var csv = [];
  var rows = dataTable.querySelectorAll("table tr");

  for(var i = 0; i < rows.length; i++) {
    var row = [], cols = rows[i].querySelectorAll("td, th");

    for(var j = 0; j < cols.length; j++) {
      if(i > 0 && j == 12) {
        row.push(cols[j].innerText.replace(',', ''));
      }
      else {
        row.push(cols[j].innerText); 
      }
    }

    csv.push(row.join(","));
  }

  downloadCSV(csv.join("\n"));
}

function downloadCSV(csv) {
  var csvFile;
  var link;

  // CSV File
  csvFile = new Blob([csv], {type: "text/csv"});

  console.log(csvFile);
  // Download Link
  link = document.createElement("a");

  // File Name
  link.download = "data.csv";

  // Create a link to the file
  link.href = window.URL.createObjectURL(csvFile);

  // Hide download link
  link.style.display = "none";

  // Add link 
  document.body.appendChild(link);

  // Click download link
  link.click();
}

var auth = firebase.auth();

function login(){

  var userEmail = document.getElementById("email_field").value;
  var userPass = document.getElementById("password_field").value;
  var list = [];

  firebase.auth().signInWithEmailAndPassword(userEmail,userPass).then(function( user) {
    firebase.database().ref().child('users').child(user.uid).once('value', function (snapshot) {

      snapshot.forEach(function(snapshot1) {
          list.push(snapshot1.val());
      });
    });

    if(list[3] != "admin") {
      firebase.auth().signOut(); 
      window.alert("This is not an admin acocunt");
    }
  }).catch(function(error) {
    // Handle Errors here.
    var errorCode = error.code;
    var errorMessage = error.message;

    window.alert("Error : " + errorMessage);
  });

}

function logout(){
  firebase.auth().signOut();
}

function openNav() {
  document.getElementById("mySidenav").style.width = "250px";
  document.getElementById("main2").style.marginLeft = "250px";
}

function closeNav() {
  document.getElementById("mySidenav").style.width = "0";
  document.getElementById("main2").style.marginLeft= "0";
}

function register() {
 //Fix this!!
 //Register user then Verify email and reset password
 //https://stackoverflow.com/questions/37517208/firebase-kicks-out-current-user
  var userEmail = document.getElementById("new_user_email").value;
  var userPass = document.getElementById("new_user_pass").value;
  var userFirstName = document.getElementById("new_user_firstname").value;
  var userLastName = document.getElementById("new_user_lastname").value;
  var isAdmin = document.getElementById("new_user_admin").checked;
  
  secondary.auth().createUserWithEmailAndPassword(userEmail, userPass).then(function(firebaseUser) {

    firebase.auth().sendPasswordResetEmail(userEmail).then(function() {
      firebaseUser.sendEmailVerification().then(function() {

        var userRef = firebase.database().ref().child('users/' + firebaseUser.uid);

        if(isAdmin) {
          userRef.set({
            email: userEmail,
            firstname: userFirstName,
            lastname: userLastName,
            type: "admin"
          });
        }
        else {
          userRef.set({
            email: userEmail,
            firstname: userFirstName,
            lastname: userLastName
          });
        }

        window.alert("User " + firebaseUser.uid + " created successfully!");
        window.alert("Reset password and verification email sent");
        document.getElementById("new_user_email").value = "";
        document.getElementById("new_user_pass").value = "";
        document.getElementById("new_user_firstname").value = "";
        document.getElementById("new_user_lastname").value = "";
        document.getElementById("new_user_admin").checked = false;

        //I don't know if the next statement is necessary 
        secondary.auth().signOut();

      }).catch(function (error) {
        window.alert(error.message);
      });
    }).catch(function (error) {
     window.alert(error.message); 
    });
    
  
  });
}

function reset_pass() {

}

function scientists() {
  document.getElementById("scientists").style.display="block";
  document.getElementById("dataView").style.display="none";
}

window.onload = function() {
  document.getElementById('cs_table').innerHTML = "";
  scientists2();
};

function scientists2() {
  var dbRef = firebase.database().ref().child('users');
  // var dbRef = document.getElementById('current_users');
  //link : https://stackoverflow.com/questions/26019279/firebase-authentication-service-lookup-uid-from-email-without-login
  //then get uid from email
  //delete user using uid

  dbRef.on('child_added', function(data) {
    var user_email = data.child("email").val();
    var user_fname = data.child("firstname").val();
    var user_lname = data.child("lastname").val();
    // var user_atype = data.child("type").val();
    if(data.child("type").val() == "admin"){
      user_atype = "admin";
    } else {
      user_atype = "";
    }
	
    document.getElementById('cs_table').innerHTML += "<tr class='tbborder'>"+
        "<th>"+user_email+"</th><th>"+user_fname+"</th><th>"+user_lname+"</th>"+ "<th>"+user_atype+"</th>"+
        // "<th><input type='submit' class='deletebtn' onclick='delete("+user_email+")' value='Delete'/></th>"+
        // "<th><input type='submit' class='deletebtn' value='Disable'/></th>"+
        // "<th><input type='submit' class='acc_btns' onclick='verify()' value='Send Verification Email'/></th>"+
        "<th><input type='submit' class='acc_btns' value='Send Reset Password Email'/></th>" + 
        "</tr>";
  });
}

function deleteUser(userEmail) {
	var dbRef = firebase.database().ref().child('users');

  admin.auth().getUserByEmail(userEmail)
  .then(function(user) {

    // See the UserRecord reference doc for the contents of user.
    alert(user.toJSON());
    console.log("Successfully fetched user data:", user.toJSON());
  })
  .catch(function(error) {
    alert("failed");
    console.log("Error fetching user data:", error);
  });
}



